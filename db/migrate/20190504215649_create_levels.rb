class CreateLevels < ActiveRecord::Migration[6.0]
  def change
    create_table :levels do |t|
      t.decimal :value, precision: 5, scale: 2

      t.timestamps
    end
  end
end
