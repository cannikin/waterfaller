class CreateWaterTemps < ActiveRecord::Migration[6.0]
  def change
    create_table :water_temps do |t|
      t.decimal :value

      t.timestamps
    end
  end
end
