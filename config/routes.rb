Rails.application.routes.draw do

  controller :graphs do
    get 'graphs' => :show, :as => :graph
  end

  controller :pages do
    get '(:span)' => :index, :as => :dashboard
  end

  namespace :api do
    namespace :v1 do
      resources :input, :only => :create
    end
  end

  root :to => 'pages#index'
end
