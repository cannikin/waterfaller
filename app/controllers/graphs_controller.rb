class GraphsController < ApplicationController

  def show
    render :json => GraphPresenter.new(params).data
  end

end
