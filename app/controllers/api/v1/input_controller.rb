module Api
  module V1
    class InputController < ApiController

      def create
        params[:event].classify.constantize.create! :value => params[:data]
        head :ok
      end

    end
  end
end
