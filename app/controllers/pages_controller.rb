class PagesController < ApplicationController

  def index
    Rails.logger.info "Host: #{request.host}"
    @presenter = DashboardPresenter.new(params)
  end

end
