import { Controller } from "stimulus"

export default class extends Controller {

  connect() {
    fetch(this.path, {
      credentials: 'same-origin'
    }).then(response => {
      if (response.ok) {
        return response.text()
      }
    }).then(html => {
      this.element.innerHTML = html
    })
  }

  get path() {
    return `/graphs/${this.data.get('type')}`
  }

}
