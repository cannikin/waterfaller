class DashboardPresenter

  attr_reader :params

  def initialize(params)
    @params = params
  end

  def spans
    ['hour', 'day', 'week']
  end

  def current_span?(a_span)
    a_span == span
  end

  def interval(span)
    case span
    when 'hour'
      'minute'
    when 'day', 'week'
      'hour'
    end
  end

  def span
    params[:span] || 'day'
  end

  def current_level
    Level.order(:created_at => :desc).limit(5).pluck(:value).reduce(:+).to_f / 5
  end

  def current_water_temp
    WaterTemp.order(:created_at => :desc).limit(5).pluck(:value).reduce(:+).to_f / 5
  end

  def current_air_temp
    AirTemp.order(:created_at => :desc).limit(5).pluck(:value).reduce(:+).to_f / 5
  end

end
