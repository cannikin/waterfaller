class GraphPresenter

  attr_reader :params

  def initialize(params)
    @params = params
  end

  def data
    models.collect do |model|
      {
        name: model.name.titleize,
        data: model.send("group_by_#{interval}", :created_at, :range => range).average(:value)
      }
    end
  end

  def models
    if params[:models]
      params[:models].collect do |model_name|
        model_name.classify.constantize
      end
    else
      [params[:model].classify.constantize]
    end
  end

  def range
    case span
    when 'hour'
      1.hour.ago..Time.now
    when 'day'
      1.day.ago..Time.now
    when 'week'
      1.week.ago..Time.now
    end

  end

  def interval
    case span
    when 'hour'
      'minute'
    when 'day', 'week'
      'hour'
    end
  end

  def span
    params[:span]
  end

end
